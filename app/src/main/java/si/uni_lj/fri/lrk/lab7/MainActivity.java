package si.uni_lj.fri.lrk.lab7;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;

import si.uni_lj.fri.lrss.machinelearningtoolkit.MachineLearningManager;
import si.uni_lj.fri.lrss.machinelearningtoolkit.classifier.Classifier;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.ClassifierConfig;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Constants;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Feature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNominal;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.FeatureNumeric;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Instance;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.MLException;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Signature;
import si.uni_lj.fri.lrss.machinelearningtoolkit.utils.Value;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";

    public static final String ACTION_CLASSIFIER_TRAINING = "si.uni_lj.fri.lrk.lab7.TRAIN_CLASSIFIER";

    AccBroadcastReceiver mBcastRecv;

    // TODO: Uncomment MachineLearningManager declaration
    MachineLearningManager mManager;

    Handler handler;

    Boolean mSensing;
    Boolean mTraining;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mBcastRecv = new AccBroadcastReceiver();

        this.handler = new Handler();

        final Button controlButton = findViewById(R.id.btn_control);

        mSensing = false;
        mTraining = false;

        controlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSensing) {
                    controlButton.setText(R.string.txt_start);
                    stopSensing();
                } else {
                    controlButton.setText(R.string.txt_stop);
                    startSensing();
                }
            }
        });

        Switch sw = findViewById(R.id.sw_training);

        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    mTraining = true;

                    findViewById(R.id.tv_select).setVisibility(View.VISIBLE);
                    findViewById(R.id.radioGroup).setVisibility(View.VISIBLE);
                    findViewById(R.id.tv_result).setVisibility(View.INVISIBLE);
                } else {
                    mTraining = false;

                    findViewById(R.id.tv_select).setVisibility(View.INVISIBLE);
                    findViewById(R.id.radioGroup).setVisibility(View.INVISIBLE);
                    findViewById(R.id.tv_result).setVisibility(View.VISIBLE);
                }
            }
        });

        initClassifier();

    }


    @Override
    protected void onStart() {
        super.onStart();

        // TODO: Register local broadcast receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mBcastRecv,
                new IntentFilter(AccBroadcastReceiver.ACTION_SENSING_RESULT));
    }


    @Override
    protected void onStop() {
        super.onStop();

        // TODO: Unregister local broadcast receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBcastRecv);

    }

    void startSensing()
    {
        Log.d(TAG,"startSensing()");

        mSensing = true;

        // TODO: set Handler to run AccSenseService every five seconds

        handler.post(new Runnable() {
            public void run() {
                Intent intent = new Intent(getApplicationContext(),
                        AccSenseService.class);
                startService(intent);
                handler.postDelayed(this, 5000);
            }
        });


    }

    void stopSensing()
    {

        Log.d(TAG,"stopSensing()");

        mSensing = false;

        this.handler.removeMessages(0);

        View v  = findViewById(R.id.container);
        v.setBackgroundColor(Color.WHITE);
        TextView t = (TextView) findViewById(R.id.tv_result);
        t.setText(R.string.txt_inferred);
    }


    void initClassifier() {
        Log.d(TAG,"initClassifier");
        try {
            mManager = MachineLearningManager.getMLManager(getApplicationContext());
        } catch (MLException e) {
            e.printStackTrace();
        }
        // TODO: Instantiate the classifier
        Feature accMean = new FeatureNumeric("accMean");
        // TODO: two more features should be defined here
        Feature accVariance = new FeatureNumeric("accVariance");
        Feature accMcr = new FeatureNumeric("accMcr");

        ArrayList<String> classValues = new ArrayList<String>();
        classValues.add(getResources().getString(R.string.txt_gesture_1));
        classValues.add(getResources().getString(R.string.txt_gesture_2));
        // TODO: one more class value should be added here
        classValues.add(getResources().getString(R.string.txt_gesture_3));

        Feature movement = new FeatureNominal("movement", classValues);
        ArrayList<Feature> features = new ArrayList<Feature>();
        features.add(accMean);
        // TODO: two other features should go here
        features.add(accVariance);
        features.add(accMcr);
        features.add(movement);
        Signature signature = new Signature(features, features.size()-1);
          // TODO: a try-catch block is needed here
        try {
            mManager.addClassifier(Constants.TYPE_NAIVE_BAYES, signature,
                    new ClassifierConfig(),"movementClassifier");
        } catch (MLException e) {
            e.printStackTrace();
        }

    }


    public void recordAccData(float mean, float variance, float MCR) {

        Log.d(TAG, "recordAccData Intensity: " + mean + " var " + variance + " MCR " + MCR);

        Switch s = findViewById(R.id.sw_training);

        if (s.isChecked()) {

            // TODO: get the label of the selected radio button

            RadioGroup rg = findViewById(R.id.radioGroup);
            int selectedId = rg.getCheckedRadioButtonId();
            RadioButton rb = (RadioButton) findViewById(selectedId);
            String label = rb.getText().toString();


            // TODO: send data to TrainClassifierService

            Intent mIntent = new Intent(MainActivity.this, TrainClassifierService.class);
            mIntent.putExtra("accMean", mean);
            mIntent.putExtra("accVar", variance);
            mIntent.putExtra("accMCR", MCR);
            mIntent.putExtra("label", label);
            mIntent.setAction(ACTION_CLASSIFIER_TRAINING);
            startService(mIntent);



        } else {

            // TODO: Do the inference (classification) and set the result (also screen background colour)
            Classifier c = mManager.getClassifier("movementClassifier");
            ArrayList<Value> instanceValues = new ArrayList<Value>();
            Value meanValue = new Value((double) mean, Value.NUMERIC_VALUE);
            instanceValues.add(meanValue);
            // TODO: add two more features here
            Value varValue = new Value((double) variance, Value.NUMERIC_VALUE);
            instanceValues.add(varValue);
            Value mcrValue = new Value((double) MCR, Value.NUMERIC_VALUE);
            instanceValues.add(mcrValue);

            // TODO: a try-catch block is needed here
            Value inference=null;
            Instance instance=new Instance(instanceValues);

            try {
                inference = c.classify(instance);
            } catch (MLException e) {
                e.printStackTrace();
            }
            if(inference.getValue().toString().equals("Rapid")){
                View v = findViewById(R.id.container);
                TextView t = (TextView) findViewById(R.id.tv_result);
                v.setBackgroundColor(Color.BLUE);
                t.setText(R.string.txt_gesture_1);
            }
            else if(inference.getValue().toString().equals("Slow")){
                View v = findViewById(R.id.container);
                TextView t = (TextView) findViewById(R.id.tv_result);
                v.setBackgroundColor(Color.RED);
                t.setText(R.string.txt_gesture_2);
            }
            else if(inference.getValue().toString().equals("Circular")){
                View v = findViewById(R.id.container);
                TextView t = (TextView) findViewById(R.id.tv_result);
                v.setBackgroundColor(Color.YELLOW);
                t.setText(R.string.txt_gesture_3);
            }

        }
    }


    public class AccBroadcastReceiver extends BroadcastReceiver {

        public static final String ACTION_SENSING_RESULT = "si.uni_lj.fri.lrk.lab7.SENSING_RESULT";
        public static final String MEAN = "mean";
        public static final String VARIANCE = "variance";
        public static final String MCR = "MCR";

        @Override
        public void onReceive(Context context, Intent intent) {

            Log.d(TAG, " AccBroadcastReceiver onReceive...");

            float mean = intent.getFloatExtra(MEAN, 0);
            float variance = intent.getFloatExtra(VARIANCE, 0);
            float mcr = intent.getFloatExtra(MCR, 0);

            Log.d(TAG, "recordAccData Intensity: " + mean + " var " + variance + " MCR " + mcr);
            recordAccData(mean, variance, mcr);
        }
    }

}
